﻿using UnityEngine;
using System.Collections;
using System.Linq;
using thelab.mvc;
using UnityEngine.UI;

public class SimonProgressbarView : SimonButtonView
{
    public Slider SimonProgressbar;
    public Text AmountText;

    public Image Icon;

    protected override void Start()
    {
        base.Start();

        var maxScore = app.model.Cards.GetById(index).amount;
        AmountText.text = maxScore.ToString();

        Icon.sprite = app.model.Cards.GetById(index).Sprite;
    }

    public override void UpdateScore(int score)
    {
        var maxScore = app.model.Scores.GetMaxScore(index);
        SimonProgressbar.value = maxScore != 0 ? (float)score / maxScore : 0;
    }
}
