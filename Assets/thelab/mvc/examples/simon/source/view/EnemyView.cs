﻿using UnityEngine;
using System.Collections;
using thelab.mvc;
using UnityEngine.UI;

public class EnemyView : View<SimonApplication>
{
    public Slider EnemyHealthSlider;
    public Text EnemyHealthText;
    public Text TurnsCountText;

    public float AfterAttackDelay = 1f;


    void Awake()
    {
        app.model.Battle.Enemy.OnEnemyHealthChanged += OnEnemyHealthChanged;
        app.model.Battle.OnTurnsNumberChanged += BattleOnOnTurnsNumberChanged;
        app.model.Battle.OnTurnsEnded += OnTurnsEnded;

        TurnsCountText.text = app.model.Battle.TurnsCount.ToString();
    }

    private void BattleOnOnTurnsNumberChanged(int i)
    {
        TurnsCountText.text = i.ToString();
    }

    private void OnTurnsEnded()
    {
        app.controller.OnTurnsNumberEnded();
    }

    private void OnEnemyHealthChanged(int newHealth)
    {
        var maxHealth = app.model.Battle.Enemy.enemyMaxHealth;
        var value = maxHealth != 0 ? (float)newHealth / maxHealth : 0;

        EnemyHealthSlider.value = value;
        EnemyHealthText.text = (value * maxHealth).ToString();
    }



    private IEnumerator EndAttackCoroutine()
    {
        yield return new WaitForSeconds(AfterAttackDelay);
        app.controller.OnBattleEnded();
    }
}
