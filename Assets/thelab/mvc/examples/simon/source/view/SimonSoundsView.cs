﻿using System.Collections.Generic;
using System.Linq;
using thelab.mvc;
using UnityEngine;

public class SimonSoundsView : View<SimonApplication>
{
    public List<AudioSource> Sounds;

    private AudioSource currentSound;

    void Start()
    {
        for (int i = 0; i < Sounds.Count; i++)
        {
            Sounds[i].clip = app.model.Cards.GetById(i).ChargeAudio;
        }
    }

    public void PlaySoundById(int id)
    {
        if (currentSound != null)
            currentSound.Stop();

        currentSound = Sounds.ElementAtOrDefault(id);
        currentSound.Play();
    }
}
