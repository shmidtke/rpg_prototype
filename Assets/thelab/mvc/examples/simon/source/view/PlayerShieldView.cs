﻿using UnityEngine;
using System.Collections;
using thelab.mvc;

public class PlayerShieldView : View<SimonApplication>
{
    public GameObject ShieldWidget;

    void Awake()
    {
        app.model.Battle.Player.Shield.OnShieldEnabled += i => ShieldWidget.SetActive(true);
        app.model.Battle.Player.Shield.OnShieldDisabled += () => ShieldWidget.SetActive(false);
    }
}
