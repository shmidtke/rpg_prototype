﻿using System;
using UnityEngine;
using System.Collections;
using thelab.mvc;
using UnityEngine.UI;

public class SimonTimerView : View<SimonApplication>
{
    public Slider TimerProgressbar;

    void Awake()
    {
        app.model.Timer.OnTimerElapsed += OnTimerElapsed;
        app.model.Timer.OnRemainingTimeChanged += OnTimer;
    }

    private void OnTimer(float timer)
    {
        var roundTime = app.model.Timer.roundTime;
        var timerProgress = Mathf.Approximately(0, roundTime) ? 0 : timer / roundTime;

        TimerProgressbar.value = timerProgress;
    }

    private void OnTimerElapsed()
    {
        app.controller.OnSimonError();
    }
}
