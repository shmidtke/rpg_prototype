﻿using System;
using thelab.mvc;
using UnityEngine.UI;

public class PlayerView : View<SimonApplication>
{
    public Slider HealthProgressbar;
    public Text HealthText;

    void Awake()
    {
        app.model.Battle.Player.OnPlayerHealthChanged += OnHealthChanged;
    }

    private void OnHealthChanged(int health)
    {
        var maxHealth = app.model.Battle.Enemy.enemyMaxHealth;
        var value = maxHealth != 0 ? (float)health / maxHealth : 0;

        HealthProgressbar.value = value;
        HealthText.text = (value * maxHealth).ToString();
    }
}
