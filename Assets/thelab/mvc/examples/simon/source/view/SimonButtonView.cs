﻿using System;
using UnityEngine;
using System.Collections;
using thelab.mvc;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SimonButtonView : View<SimonApplication>
{
    //Индекс кнопки (0..3)
    public int index;

    public Color NormalColor;
    public Color HighlightedColor;

    public float HighlightTime = 0.25f;

    protected virtual void Awake()
    {
        button.onClick.AddListener(OnSimonButtonClick);
    }

    private void OnSimonButtonClick()
    {
        app.view.OnButtonClick(index);
    }

    protected virtual void Start()
    {
        image.color = NormalColor;
    }

    /// <summary>
    /// Подсветить кнопку
    /// </summary>
    public void Highlight()
    {
        image.color = HighlightedColor;
    }

    /// <summary>
    /// Потушить кнопку
    /// </summary>
    public void Dim()
    {
        button.targetGraphic.color = NormalColor;
    }

    /// <summary>
    /// Обновление очков данной кнопки саймона
    /// </summary>
    public virtual void UpdateScore(int score)
    {

    }

    private Button _b;

    public Button button
    {
        get
        {
            if (_b == null)
            {
                _b = gameObject.GetComponent<Button>();
            }

            return _b;
        }
    }

    private Image _image;

    private Image image
    {
        get
        {
            if (_image == null)
            {
                _image = gameObject.GetComponent<Image>();
            }

            return _image;
        }
    }
}
