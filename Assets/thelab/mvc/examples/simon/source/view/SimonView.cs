﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using thelab.mvc;
using UnityEngine.UI;

public class SimonView : View<SimonApplication>
{
    //Кнопка начала игры
    public Button StartButton;

    //Список кнопок саймона
    public List<SimonButtonView> Buttons = new List<SimonButtonView>();

    //Время, на которое подсвечивается кнопка
    public float HighlightTime;
    //Задержка между свечениями
    public float HighlightDelay;
    //Задержка подсветок кнопок после ввода
    public float AfterInputHighlightDelay;
    //Задержка при выводе ошибки
    public float ErrorDelay;
    public float StartGameDelay = 1f;

    public Text InfoLabel;

    public SimonSoundsView SoundsView;

    void Awake()
    {
        StartButton.onClick.AddListener(StartGame);

        app.model.OnCombinationPlayStarted += PlayCombination;
        app.model.OnInputBegin += BeginInput;
        app.model.OnCombinationError += OnError;
        app.model.OnCombinationInputComplete += OnCombinationInputComplete;
        app.model.OnBattleBegin += OnBattleBegin;

        app.model.Scores.OnScoresUpdated += OnScoresUpdated;

        SetButtonsInteractable(false);
    }

    public void OnButtonClick(int id)
    {
        StopAllCoroutines();
        SoundsView.PlaySoundById(id);
        app.controller.OnSimonButtonPressed(id);
    }

    /// <summary>
    /// Событие начала боя
    /// </summary>
    private void OnBattleBegin()
    {
        SetButtonsInteractable(true);
        StartButton.interactable = true;
        InfoLabel.text = "Ходи";
    }

    /// <summary>
    /// Событие обновления очков кнопки саймона
    /// </summary>
    private void OnScoresUpdated(int index, int score)
    {
        var button = Buttons.FirstOrDefault(b => b.index == index);
        if (button == null) return;

        button.UpdateScore(score);
    }

    /// <summary>
    /// Начало игры
    /// </summary>
    private void StartGame()
    {
        StartButton.interactable = false;
        StopAllCoroutines();
        StartCoroutine(StartGameCoruotine());
    }

    /// <summary>
    /// Реакция на завершение ввода комбинации
    /// </summary>
    private void OnCombinationInputComplete(SimonCombination obj)
    {
        SetButtonsInteractable(false);
        StartCoroutine(CompleteCoroutine());
    }

    /// <summary>
    /// Реакция на ошибку ввода
    /// </summary>
    /// <param name="obj"></param>
    private void OnError(SimonCombination obj)
    {
        StartCoroutine(DisplayErrorCoroutine());

        InfoLabel.text = "Ошибка";
    }

    /// <summary>
    /// Событие завершения проигрывания комбинации
    /// </summary>
    private void BeginInput(SimonCombination combination)
    {
        StopAllCoroutines();
        SetButtonsInteractable(true);
        SetButtonsHighlighted(false);

        InfoLabel.text = "Тыкай";
    }

    /// <summary>
    /// Проиграть комбинацию саймона
    /// </summary>
    public void PlayCombination(SimonCombination combination)
    {
        SetButtonsInteractable(false);
        SetButtonsHighlighted(false);

        StopAllCoroutines();
        StartCoroutine(CombinationPlayingCoroutine(combination));

        InfoLabel.text = "Смотри";
    }

    /// <summary>
    /// Задать активность кнопок
    /// </summary>
    public void SetButtonsInteractable(bool interactable)
    {
        foreach (var b in Buttons) b.button.interactable = interactable;
    }

    /// <summary>
    /// Задать свечение кнопкам
    /// </summary>
    /// <param name="b"></param>
    public void SetButtonsHighlighted(bool b)
    {
        foreach (var button in Buttons)
        {
            if (b) button.Highlight();
            else
                button.Dim();
        }
    }

    #region coroutines
    private IEnumerator CombinationPlayingCoroutine(SimonCombination combination)
    {
        foreach (var element in combination.GetCombination())
        {
            var button = Buttons.FirstOrDefault(b => b.index == element);
            if (button == null)
            {
                Debug.LogError("Cannot get button with index " + element);
                continue;
            }

            button.Highlight();
            SoundsView.PlaySoundById(element);
            yield return new WaitForSeconds(HighlightTime);
            button.Dim();
            yield return new WaitForSeconds(HighlightDelay);
        }

        app.controller.OnSimonPlayComplete();
    }

    private IEnumerator DisplayErrorCoroutine()
    {
        SetButtonsInteractable(false);

        yield return new WaitForSeconds(1f);
        foreach (var button in Buttons)
        {
            button.Highlight();
            yield return new WaitForSeconds(0.1f);
            button.Dim();
        }

        app.controller.OnErrorDisplayComplete();
    }

    private IEnumerator CompleteCoroutine()
    {
        yield return new WaitForSeconds(1f);
        app.controller.OnSimonInputComplete();
    }

    private IEnumerator StartGameCoruotine()
    {
        yield return new WaitForSeconds(StartGameDelay);

        app.controller.StartGame();
        app.model.Battle.DecreaseTurnsNumber();
    }
    #endregion

    void OnDestroy()
    {
        if (app == null || app.model == null) return;

        app.model.OnCombinationPlayStarted -= PlayCombination;
        app.model.OnInputBegin -= BeginInput;
        app.model.OnCombinationError -= OnError;
    }
}
