﻿using UnityEngine;
using System.Collections;
using thelab.mvc;

public class SimonController : Controller<SimonApplication>
{
    public void StartGame()
    {
        app.model.Combinator.Reset();
        app.model.Timer.ResetTimer();
        SetSimonState(SimonState.Play);
    }

    /// <summary>
    /// Обработка события нажатия кнопки саймона
    /// </summary>
    /// <param name="buttonNumber"></param>
    public void OnSimonButtonPressed(int buttonNumber)
    {
        switch (app.model.state)
        {
            case SimonState.Input:
                if (app.model.Combinator.GetNextElement() == buttonNumber)
                {
                    app.model.Scores.AddScores(buttonNumber);

                    if (app.model.Combinator.IsCombinationComplete)
                    {
                        SetSimonState(SimonState.Complete);
                        app.model.Timer.StopTimer();
                        app.model.Timer.ResetTimer();
                    }
                }
                else
                {
                    OnSimonError();
                }
                break;
            case SimonState.Battle:
                if (app.model.Scores.GetScore(buttonNumber) < app.model.Scores.GetMaxScore(buttonNumber))
                    break;

                var amount = app.model.Cards.GetById(buttonNumber).amount;
                var card = app.model.Cards.GetById(buttonNumber);

                switch (card.Type)
                {
                    case CardType.Attack:
                        app.model.Battle.Enemy.AvoidDamage(amount);
                        break;
                    case CardType.Heal:
                        app.model.Battle.Player.Heal(amount);
                        break;
                    case CardType.Defence:
                        app.model.Battle.Player.Shield.EnableShiled(amount);
                        break;
                }
                app.model.Scores.ResetScore(buttonNumber);
                app.model.Battle.DecreaseTurnsNumber();

                break;
        }

    }

    public void OnSimonPlayComplete()
    {
        SetSimonState(SimonState.Input);
        app.model.Timer.ActivateTimer();
    }

    public void OnSimonInputComplete()
    {
        SetSimonState(SimonState.Play);
    }

    public void OnErrorDisplayComplete()
    {
        SetSimonState(SimonState.Battle);
    }

    public void OnBattleEnded()
    {
        app.model.Combinator.Reset();
        SetSimonState(SimonState.Play);
    }

    public void OnTurnsNumberEnded()
    {
        app.model.Battle.Enemy.EnemyAttack();
        app.model.Battle.ResetTurnsCount();
    }

    public void OnSimonError()
    {
        SetSimonState(SimonState.Error);
        app.model.Timer.StopTimer();
    }

    private void SetSimonState(SimonState state)
    {
        app.model.state = state;
    }
}
