﻿using System;
using System.CodeDom;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using thelab.mvc;

public class SimonModel : Model<SimonApplication>
{
    public event Action<SimonCombination> OnCombinationPlayStarted = delegate { };
    public event Action<SimonCombination> OnInputBegin = delegate { };

    public event Action<SimonCombination> OnCombinationError = delegate { };
    public event Action<SimonCombination> OnCombinationInputComplete = delegate { };

    public event Action OnBattleBegin = delegate { };

    //Количество кнопок саймона
    public int ButtonsCount;
    //Стартовая длина комбинации
    public int CombinationStartLength = 1;
    //"Прирост" комбинации
    public int CombinationGrowAmount = 1;
    //Модель для работы с комбинациями
    public SimonCombinator Combinator;
    //Модель для работы с очками
    public SimonScoresModel Scores;
    //Модель для работы с битвой
    public SimonBattleModel Battle;
    //Модель таймера
    public SimonTimerModel Timer;

    public SimonCardsModel Cards;

    private SimonState _state;
    public SimonState state
    {
        get { return _state; }
        set
        {
            if (_state == value) return;
            _state = value;

            switch (value)
            {
                case SimonState.Play:
                    OnCombinationPlayStarted(Combinator.currentCombination);
                    break;
                case SimonState.Input:
                    OnInputBegin(Combinator.currentCombination);
                    break;
                case SimonState.Error:
                    Combinator.Reset();
                    OnCombinationError(Combinator.currentCombination);
                    break;
                case SimonState.Complete:
                    app.model.Combinator.currentCombination.Grow(CombinationGrowAmount);
                    Combinator.Complete();
                    OnCombinationInputComplete(Combinator.currentCombination);
                    break;
                case SimonState.Battle:
                    OnBattleBegin();
                    break;
            }
        }

    }
}

public enum SimonState
{
    None,
    Play,
    Input,
    Complete,
    Error,
    Battle,
}
