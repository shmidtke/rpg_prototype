﻿using System;
using UnityEngine;
using System.Collections;
using thelab.mvc;

public class SimonTimerModel : Model<SimonApplication>
{
    public event Action OnTimerActivated = delegate { };
    public event Action OnTimerElapsed = delegate { };
    public event Action<float> OnRemainingTimeChanged = delegate { };
    public event Action OnTimerStopped = delegate { };

    public float roundTime;

    [SerializeField]
    private float _remainingTime;

    private float remainingTime
    {
        get { return _remainingTime; }
        set
        {
            if (Mathf.Approximately(_remainingTime, value)) return;
            _remainingTime = value;
            OnRemainingTimeChanged(_remainingTime);
        }
    }

    private Coroutine timerCoroutine;

    void Start()
    {
        ResetTimer();
    }

    public void ActivateTimer()
    {
        OnTimerActivated();

        timerCoroutine = StartCoroutine(TimerCoroutine());
    }

    public void StopTimer()
    {
        if (timerCoroutine != null)
            StopCoroutine(timerCoroutine);

        OnTimerStopped();
    }

    public void ResetTimer()
    {
        remainingTime = roundTime;
    }

    private IEnumerator TimerCoroutine()
    {
        while (remainingTime > 0)
        {
            remainingTime -= Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        OnTimerElapsed();
    }
}
