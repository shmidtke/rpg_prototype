﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using thelab.mvc;

public class SimonScoresModel : Model<SimonApplication>
{
    public event Action<int, int> OnScoresUpdated = delegate { };

    private Dictionary<int, int> _scores = new Dictionary<int, int>();

    /// <summary>
    /// Добавление очков по индексу
    /// </summary>
    /// <param name="index"></param>
    public void AddScores(int index)
    {
        var score = _scores.ContainsKey(index) ? _scores[index] + 1 : 1;

        SetScore(index, score);
    }

    /// <summary>
    /// Получение очков по индексу
    /// </summary>
    /// <param name="index"></param>
    public int GetScore(int index)
    {
        int score;
        _scores.TryGetValue(index, out score);
        return score;
    }

    /// <summary>
    /// Сброс очков по индексу
    /// </summary>
    /// <param name="index"></param>
    public void ResetScore(int index)
    {
        SetScore(index, 0);
    }

    public int GetMaxScore(int index)
    {
        return app.model.Cards.GetById(index).capacity;
    }

    private void SetScore(int index, int value)
    {
        value = Mathf.Clamp(value, 0, GetMaxScore(index));

        if (_scores.ContainsKey(index))
            _scores[index] = value;
        else
            _scores.Add(index, value);

        OnScoresUpdated(index, _scores[index]);
    }
}
