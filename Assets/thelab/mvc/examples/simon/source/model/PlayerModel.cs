﻿using System;
using UnityEngine;
using System.Collections;
using thelab.mvc;

public class PlayerModel : Model<SimonApplication>
{
    public event Action<int> OnPlayerHealthChanged = delegate { };
    public event Action<int> OnPlayerDamaged = delegate { };

    public int MaxHealth;

    public PlayerShieldModel Shield;

    private int _health;
    public int health
    {
        get { return _health; }
        set
        {
            if (_health == value) return;

            _health = value;
            OnPlayerHealthChanged(_health);
        }
    }

    void Start()
    {
        ResetHealth();
    }

    public void Damage(int damage)
    {
        if (damage <= 0) return;

        health = Mathf.Max(0, health - (damage - Shield.amount));
        Shield.DisableShield();
        OnPlayerDamaged(damage);
    }

    public void Heal(int hp)
    {
        if (hp <= 0)
            return;

        health = Mathf.Min(MaxHealth, health + hp);
    }

    void ResetHealth()
    {
        health = MaxHealth;
    }
}
