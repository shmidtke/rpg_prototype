﻿using System;
using UnityEngine;
using System.Collections;
using thelab.mvc;

public class SimonBattleModel : Model<SimonApplication>
{
    public event Action OnTurnsEnded = delegate { };
    public event Action<int> OnTurnsNumberChanged = delegate { };
    
    public int TurnsCount;

    private int _turnsNumber;

    //Модель для работы с врагом
    public EnemyModel Enemy;
    //Модель для работы с игроком
    public PlayerModel Player;

    void Start()
    {
        ResetTurnsCount();
    }


    public int turnsNumber
    {
        get { return _turnsNumber; }
        set
        {
            if (_turnsNumber == value)
                return;

            _turnsNumber = value;
            OnTurnsNumberChanged(_turnsNumber);
        }
    }

    public void DecreaseTurnsNumber()
    {
        turnsNumber = Mathf.Max(0, turnsNumber - 1);

        if (turnsNumber == 0)
            OnTurnsEnded();
    }


    public void ResetTurnsCount()
    {
        turnsNumber = TurnsCount;
    }
}
