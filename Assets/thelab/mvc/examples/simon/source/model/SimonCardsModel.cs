﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using thelab.mvc;

public class SimonCardsModel : Model<SimonApplication>
{
    [SerializeField]
    private List<SimonCard> Cards;

    public SimonCard GetById(int id)
    {
        return Cards.ElementAtOrDefault(id);
    }
}

[Serializable]
public class SimonCard
{
    public CardType Type;
    public int capacity;
    public int amount;
    public Sprite Sprite;
    public AudioClip ChargeAudio;
}

public enum CardType
{
    Attack,
    Defence,
    Heal,
}
