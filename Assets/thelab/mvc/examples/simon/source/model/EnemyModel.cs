﻿using System;
using UnityEngine;
using System.Collections;
using thelab.mvc;

public class EnemyModel : Model<SimonApplication>
{
    public event Action<int> OnEnemyHealthChanged = delegate (int i) { };

    public int damage;

    public int enemyMaxHealth;
    private int enemyHealth;

    void Start()
    {
        ResetHealth();
    }

    /// <summary>
    /// Сброс здоровья к максимальному
    /// </summary>
    public void ResetHealth()
    {
        enemyHealth = enemyMaxHealth;
    }

    /// <summary>
    /// Нанести урон
    /// </summary>
    public void AvoidDamage(int damage)
    {
        if (damage <= 0)
            return;

        enemyHealth = Mathf.Max(0, enemyHealth - damage);
        OnEnemyHealthChanged(enemyHealth);
    }

    public void EnemyAttack()
    {
        app.model.Battle.Player.Damage(damage);
    }
}
