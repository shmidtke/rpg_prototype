﻿using System;
using UnityEngine;
using System.Collections;
using System.Xml.Schema;
using thelab.mvc;

public class PlayerShieldModel : Model<SimonApplication>
{
    public event Action<int> OnShieldEnabled = delegate { };
    public event Action OnShieldDisabled = delegate { };
    public int amount { get; private set; }

    public void EnableShiled(int amount)
    {
        this.amount = amount;
        OnShieldEnabled(amount);
    }

    public void DisableShield()
    {
        amount = 0;
        OnShieldDisabled();
    }
}
