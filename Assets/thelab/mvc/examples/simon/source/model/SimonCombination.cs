﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using thelab.mvc;
using Random = UnityEngine.Random;

/// <summary>
/// Класс, хранящий комбинацию саймона, и позволяющий совершать с ней какие-либо действия
/// </summary>
[Serializable]
public class SimonCombination
{
    [SerializeField]
    private List<int> _combination = new List<int>();
    private int maxElementIndex = 3;

    public SimonCombination(int count, int buttonsCount = 4)
    {
        maxElementIndex = buttonsCount - 1;

        _combination = new List<int>();
        for (int i = 0; i < count; i++)
        {
            AddRandom();
        }
    }

    public SimonCombination(IEnumerable<int> combination)
    {
        _combination = combination.ToList();
    }

    /// <summary>
    /// Получить комбинацию кнопок саймона
    /// </summary>
    public IEnumerable<int> GetCombination()
    {
        return _combination.Where(e => e <= maxElementIndex);
    }

    /// <summary>
    /// Получить элемент комбинации по индексу
    /// </summary>
    public int GetElement(int index)
    {
        var combination = GetCombination();
        if (index > combination.Count())
        {
            Debug.LogWarning("Try to get combination element out of bounds");
            return -1;
        }

        return combination.ElementAt(index);
    }

    /// <summary>
    /// Увеличить комбинацию
    /// </summary>
    public void Grow(int amount = 1)
    {
        AddRandom(amount);
    }

    /// <summary>
    /// Добавить конкретный элемент
    /// </summary>
    public void AddElement(int element)
    {
        if (element > maxElementIndex)
        {
            Debug.LogWarning(string.Format("Try to set combination index to {0}: max index - {1}", element, maxElementIndex));
            return;
        }
        _combination.Add(element);
    }

    /// <summary>
    /// Добавить 1 или несколько рандомных элементов
    /// </summary>
    private void AddRandom(int count = 1)
    {

        for (int i = 0; i < count; i++)
        {
            _combination.Add(GetRandomElement());
        }
    }

    private int GetRandomElement()
    {
        if (!_combination.Any())
            return Random.Range(0, maxElementIndex + 1);

        var usingDictionary = new Dictionary<int, int>();

        foreach (var element in _combination)
        {
            if (usingDictionary.ContainsKey(element))
            {
                usingDictionary[element]++;
            }
            else
            {
                usingDictionary.Add(element, 1);
            }
        }

        var exceptingElement = usingDictionary.OrderByDescending(kvp => kvp.Value).FirstOrDefault();

        int result = Random.Range(0, maxElementIndex + 1);
        while (result == exceptingElement.Key)
        {
            result = Random.Range(0, maxElementIndex + 1);
        }

        return result;
    }
}
