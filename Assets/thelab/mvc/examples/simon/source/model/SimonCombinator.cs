﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using thelab.mvc;

public class SimonCombinator : Model<SimonApplication>
{
    public List<SimonCombination> PresetCombinations = new List<SimonCombination>();

    private SimonCombination _currentCombination;
    public SimonCombination currentCombination
    {
        get
        {
            if (_currentCombination == null)
            {
                if (!PresetCombinations.Any())
                    _currentCombination = new SimonCombination(app.model.CombinationStartLength);
                else
                    _currentCombination = PresetCombinations.FirstOrDefault();
            }

            return _currentCombination;
        }
    }

    /// <summary>
    /// Индеск текущего элемента комбинации
    /// </summary>
    private int index;


    public int GetNextElement()
    {
        if (IsCombinationComplete)
        {
            Debug.LogWarning("Try to get next element in completed combination");
            return -1;
        }

        var element = currentCombination.GetCombination().ElementAt(index);
        index++;

        return element;
    }

    public void Complete()
    {
        index = 0;
    }

    public void Reset()
    {
        _currentCombination = null;
        index = 0;
    }

    public bool IsCombinationComplete
    {
        get { return index >= currentCombination.GetCombination().Count(); }
    }
}
